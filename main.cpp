// C++ Code
#include <iostream>
using namespace std;

void max(int arr[],int size)
{
    int large = 0;
    for (int i = 0; i < size; i++)
    {
        if (arr[i] > large)
        {
            large = arr[i];
        }
    }

    cout << large;
}

int main()
{
    int arr[] = {23, 546, 89};
    int size = sizeof(arr) / sizeof(arr[0]);
    max(arr,size);
}
